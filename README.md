# Rockboxed iPod Syncing Script

A simple script that I wrote to keep my iPod and music library in sync.

There is some funny behaviour when using Rockbox and the iFlash-Quad microSD card adaptor where some songs will repeatedly skip. This is solved by only copying music over when your iPod is in disk mode.
