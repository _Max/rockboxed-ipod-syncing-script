#!/bin/sh

LIBRARY=${1:-/home/max/Music/}
IPOD=${2:-/media/ipod/Music/}

echo Copying files from $IPOD to $LIBRARY
rsync -av --ingore-existing $IPOD $LIBRARY
echo Successful sync from $IPOD to $LIBRARY

echo Copying files from $LIBRARY to $IPOD
rsync -av --ignore-existing $LIBRARY $IPOD
echo Successful sync from $LIBRARY to $IPOD


